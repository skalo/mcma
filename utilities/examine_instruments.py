import csv
import glob
import math
import os
import re
import zipfile

from lxml import etree

from change_instruments import DEFAULT_INSTRUMENT, INSTRUMENT_SEPARATOR
from midi_instrument import get_exact_instrument, get_midi_progrma_to_instrument_names
from xml_utils import get_child, get_text



base_folder = os.path.join('..', 'mcma')

# Update this folder as needed.
music_folder = os.path.join(base_folder, 'lully_jb', 'persee')

# Valid instrument name characters.
regex_instrument_names = re.compile('[^a-zA-Z\s]')


def main():

    file_to_instruments = {}

    for f in glob.glob(f'{music_folder}/*.mxl'):
        file_id, _ = os.path.splitext(os.path.basename(f))
        archive = zipfile.ZipFile(f)  # manages the zip file

        # Process the first XML file in the archive that isn't "container.xml".
        for f2 in archive.filelist:
            basename = os.path.basename(f2.filename)
            base, ext = os.path.splitext(basename)
            if basename != 'container.xml' and ext == '.xml':
                instruments = examine_xml(archive.open(f2), file_id)
                file_to_instruments[file_id] = instruments
                break
        else:
            print(f"WARNING: Didn't find XML file inside {f}")

    # Now read the metadata.csv and display the results in the same order as file ids occur there.
    metadata_path = os.path.join(music_folder, 'metadata.csv')
    if not os.path.exists(metadata_path):
        print('Warning: no metadata.csv detected. Files may be in a different order than expected.')
        for k, v in sorted(file_to_instruments.items()):
            print(f'{k}, {v}')
    else:
        print('Displaying suggested instruments column to paste into metadata.csv:')
        with open(metadata_path) as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # skip header
            ids = [row[0] for row in reader]
        for file_id in ids:
            print(f'{file_id}, {file_to_instruments[file_id]}')


def clean_instrument_name(original_name, file_id, midi_program=None):
    """Return a good instrument name to use, from the current name and a midi program #"""
    # Remove all invalid characters (such as numbers) from XML instrument name.
    name = regex_instrument_names.sub('', original_name).strip()

    # Look up instrument.
    instrument = get_exact_instrument(name)

    if instrument:
        return name

    # Instrument not found. Try to guess a good one using the midi program number, but give warning.
    valid_midi_program = False
    try:
        midi_program = int(midi_program)
        if 1 <= midi_program <= 128:
            valid_midi_program = True
    except ValueError:
        pass

    if not valid_midi_program:
        print(f'WARNING: [{file_id}] Unknown instrument "{original_name}". '
              f'Defaulting to {DEFAULT_INSTRUMENT.name}')

    names = get_midi_progrma_to_instrument_names()[midi_program]
    name = names[0]
    choices_text = ''
    if len(names) > 1:
        choices_text = f' from choices {names}'
    print(f'WARNING: [{file_id}] Unknown instrument "{original_name}". '
          f'Replacing with {name}{choices_text}')
    return name


def examine_xml(f, file_id):
    """Returns the string to be used in the metadata.csv Instruments column for this file."""
    #print(f'Examining file {file_id}')
    # Parse XML.
    tree = etree.parse(f)
    root = tree.getroot()

    instruments = []

    parts = root.findall('.//score-part')
    for part_number, part in enumerate(parts):
        score_instrument = get_child(part, 'score-instrument', create_if_missing=False)
        instrument_name = get_child(score_instrument, 'instrument-name')

        midi_instrument = get_child(part, 'midi-instrument')
        midi_program = get_child(midi_instrument, 'midi-program')

        # Use some logic to convert this name into a reasonable instrument. Use the midi program
        # as a suggestion if instrument is unknown.
        instrument_name = clean_instrument_name(get_text(instrument_name), file_id, get_text(midi_program))
        instruments.append(instrument_name)

    insturment_str = INSTRUMENT_SEPARATOR.join(instruments)
    #print(f'{file_id}, {insturment_str}')

    return insturment_str

if __name__ == '__main__':
    main()