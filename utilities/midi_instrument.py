from collections import defaultdict
from copy import copy
import csv
from dataclasses import dataclass

from levenshtein import levenshtein


@dataclass(eq=True, frozen=True, order=True)
class MidiInstrument:
    program: int
    name: str
    alternate_names: list
    volume_boost: float = 0.0

    def all_names(self):
        names = [self.name]
        names.extend(self.alternate_names)
        return names

    @staticmethod
    def _from_row(row):
        program = int(row[0])
        name = row[1]
        alt_names = ""
        volume_boost = 0.0
        if len(row) > 2:
            alt_names = row[2].split(',')
            if len(row) > 3:
                volume_boost = float(row[3])
        return MidiInstrument(program, name, alt_names, volume_boost)

    @staticmethod
    def _read_instruments():
        with open('midi_instruments.csv') as csvfile:
            reader = csv.reader(csvfile)
            next(reader)  # skip header
            instruments = [MidiInstrument._from_row(row) for row in reader]
        return instruments


def _build_instruments_dict(instruments):
    """Build a dict from lowercase name to instrument, for all instrument names and alternate names."""
    d = {}
    for i in instruments:
        d[i.name.lower()] = i
        for name in i.alternate_names:
            # Make a new MidiInstrument with this alternate name as its actual name.
            # Replace the current name in the alternate names list with the regular name.
            alternate_names = copy(i.alternate_names)
            alternate_names.remove(name)
            alternate_names.insert(0, i.name)
            d[name.lower()] = MidiInstrument(i.program, name, alternate_names, i.volume_boost)
    return d


_INSTRUMENTS = MidiInstrument._read_instruments()
_LOWER_NAME_TO_INSTRUMENT = _build_instruments_dict(_INSTRUMENTS)

# TODO: Validate CSV file. Make sure that each name only shows up once, across all names/alternate names.
# If a name is duplicated, behavior is undefined.
# Also validate that all MIDI channels from 1-128 are included.


def get_instrument(name):
    """Returns the known midi instrument with the closest name to "name", measured by edit distance."""
    name = name.lower()
    tuples = [(levenshtein(name, n), instrument) for n, instrument in _LOWER_NAME_TO_INSTRUMENT.items()]
    return sorted(tuples)[0][1]


def get_exact_instrument(name):
    """Returns the known midi instrument only if it is an exact match (in lowercase)."""
    name = name.lower()
    return _LOWER_NAME_TO_INSTRUMENT.get(name)


def get_all_isnturment_names():
    """Return a list of all known instrument names."""
    raise NotImplementedError


def get_midi_progrma_to_instrument_names():
    """Return a dict mapping midi program number to a list of all instrument names for this program."""
    d = defaultdict(list)
    for i in _INSTRUMENTS:
        program = i.program
        for name in i.all_names():
            d[program].append(name)

    return d