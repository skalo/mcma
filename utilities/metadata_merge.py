import glob
import pandas as pd

'''
Run this script from the current directory (mcmca/utilities).
In your terminal, type:
$ python metadata_merge.py

This will create a master_metadata.csv in the current directory
'''

MCMA_PATH = '../mcma'

def main():
	col_names = ['ID', 'Last Name',	'First Name', 'Title', 'Collection', 'Catalog Number',
		         'Movement Number',	'Number of Parts', 'Instruments',	'Year',	'Provenance',
	             'Reference Edition', 'Link to Reference Edition', 'Comments']

	master_metadata = pd.DataFrame(columns=col_names)

	for metadata in glob.glob(MCMA_PATH+'/**/*.csv', recursive=True):
	    meta = pd.read_csv(metadata)
	    master_metadata = master_metadata.append(meta, ignore_index=True, sort=False)

	master_metadata = master_metadata.reindex(columns=col_names)
	master_metadata = master_metadata.sort_values(by=['ID'], ascending = True)
	master_metadata.to_csv('master_metadata.csv', index=False)


if __name__ == "__main__":
    main()
