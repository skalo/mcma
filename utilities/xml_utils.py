from lxml.etree import Element


def make_child(parent, child_tag, child_text=''):
    """Make a new element, append to parent, and return the new element."""
    node = Element(child_tag)
    if child_text:
        node.text = str(child_text)
    parent.append(node)
    return node


def get_child(parent, child_name, text='', create_if_missing=False):
    child = parent.find(child_name)
    if child is None and create_if_missing:
        child = make_child(parent, child_name, text)
    return child


def clean_child_text(parent, child_name, text='', create_if_missing=True):
    child = get_child(parent, child_name, create_if_missing=create_if_missing)
    if child is not None and text:
        child.text = text
    return child


def remove_child(parent, child_name):
    child = parent.find(child_name)
    if child is not None:
        parent.remove(child)


def replace_child(parent, child_name):
    """Remove the named child if it exists, and create a new one in its place. Return the new child."""
    remove_child(parent, child_name)
    return make_child(parent, child_name)


def get_text(node):
    if node is None:
        return ''
    return node.text