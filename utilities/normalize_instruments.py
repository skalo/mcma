import glob
import math
import os
import zipfile

from lxml import etree

import pandas as pd

from midi_instrument import get_instrument
from xml_utils import make_child, get_child, clean_child_text, remove_child, replace_child


# TODO: Rename the xml file inside the archive to f.path.replace(".mxl", ".xml"), for consistency.
# TODO: Implement something that changes the container.xml content to redirect to the new file name

# Character used to separate multiple instruments in CSV metadata.
INSTRUMENT_SEPARATOR = ';'

# Instrument to use if not specified in metadata.
DEFAULT_INSTRUMENT = get_instrument("Harpsichord")

# Default MusicXML volume level (range: 0-100).
DEFAULT_VOLUME = 80

# Pan spread setting.
DEFAULT_PAN_BORDER_DISTANCE = 30

# TODO: Add option to overwrite instead of write to "normalized" folder.


def main():
    # Please read instructions at the top of the page
    base_folder = os.path.join('..', 'mcma')

    #music_folder = os.path.join(base_folder, 'bach_js', 'well_tempered_clavier', 'book_I')

    for root, dirs, files in os.walk(base_folder):
        # Exclude hidden directories.
        dirs[:] = [d for d in dirs if not d.startswith('.')]

        # Process any directories with a metadata.csv file.
        for dir in dirs:
            subdir = os.path.join(root, dir)
            if os.path.exists(os.path.join(subdir, 'metadata.csv')):
                process_folder(subdir)


def process_folder(music_folder):
    print(f'\nProcessing folder {music_folder}...')
    output_folder = os.path.join(music_folder, 'normalized')
    assert(os.path.exists(music_folder))

    # read metaata.
    metadata_path = os.path.join(music_folder, 'metadata.csv')
    assert os.path.exists(metadata_path), f'metadata.csv must exist in music path {music_folder}'
    df_metadata = pd.read_csv(metadata_path)

    os.makedirs(output_folder, exist_ok=True)
    for f in glob.glob(f'{music_folder}/*.mxl'):
        file_id, _ = os.path.splitext(os.path.basename(f))
        archive = zipfile.ZipFile(f)  # manages the zip file

        # Process the first XML file in the archive that isn't "container.xml".
        for f2 in archive.filelist:
            basename = os.path.basename(f2.filename)
            base, ext = os.path.splitext(basename)
            if basename != 'container.xml' and ext == '.xml':
                new_xml_filepath = process_xml(archive.open(f2), file_id, output_folder, df_metadata)
                compress_to_mxl(archive, f2, new_xml_filepath)
                break
        else:
            print(f"WARNING: Didn't find XML file inside {f}")


def compress_to_mxl(zin, old_xml_filename, new_xml_filepath):
    """Given the original zipfile, replace the XML file inside with the new one that exists at new_xml_filepath."""

    # Convert the .xml path to the .mxl path.
    mxl_path = new_xml_filepath[:-3] + 'mxl'
    new_xml_basename = os.path.basename(new_xml_filepath)

    # Copy the existing zip archive but skip the musicxml file.
    with zipfile.ZipFile(mxl_path, 'w') as zout:
        zout.comment = zin.comment # preserve the comment
        for item in zin.infolist():
            if item.filename != new_xml_basename:
                zout.writestr(item, zin.read(item.filename))

    # Now add the new xml file to the mxl archive.
    with zipfile.ZipFile(mxl_path, mode='a', compression=zipfile.ZIP_DEFLATED) as zout:
        zout.write(new_xml_filepath, new_xml_basename)

    # Delete the xml file.
    os.remove(new_xml_filepath)


def xpp(xml_element):
    """XML pretty-print"""
    print(etree.tostring(xml_element, pretty_print=True).decode("utf-8"))


def process_xml(f, file_id, output_folder, df_metadata):
    """Process the XML file and write the resulting .xml file to the output folder.

    Replaces instrument details with those specified in the metadata.
    """
    print(f'Processing file {file_id}')

    # Look up desired instruments in metadata.
    metadata_row = df_metadata.loc[df_metadata['ID'] == file_id]
    if metadata_row.empty:
        raise Exception(f'File ID {file_id} not found in metadata.')
    row_dict = metadata_row.to_dict('records')[0]

    instrument_names = row_dict['Instruments'].split(INSTRUMENT_SEPARATOR)
    instruments = [get_instrument(s) for s in instrument_names]

    # Parse XML and then modify it.
    tree = etree.parse(f)
    root = tree.getroot()

    # Clean up the score's metadata and title.
    clean_score_header_and_title(root, row_dict)

    # Assume all separate parts we care about as instruments show up in score-part elements.
    parts = root.findall('.//score-part')

    # Count parts and compare the the # of instruments in the metadata.
    # If only one instrument specified and multiple parts, clone it for all parts.
    # If too few parts, just ignore the extra insturments in the metadata.
    # If too many parts, warn and fill in extras a the end with default instrument set globally.
    num_parts = len(parts)
    num_instruments = len(instruments)
    if num_parts != num_instruments:
        if num_instruments == 1:
            instruments = [instruments[0]] * num_parts
        else:
            print(f'Warning: different # of parts [{num_parts}] vs # instruments [{num_instruments}] '
                  f'in metadata for file {file_id}. '
                  'Setting Harpsichord default.')
            for i in range(num_parts - num_instruments):
                instruments.append(DEFAULT_INSTRUMENT)
        num_instruments = len(instruments)
        assert num_parts == num_instruments

    for part_number, part in enumerate(parts):
        # Choose a midi channel to use, but skip channel 10
        midi_channel_num = part_number + 1 if part_number + 1 < 10 else part_number + 2
        clean_part(part, part_number, num_instruments, midi_channel_num, instruments[part_number])

    etree.indent(root)
    outfile_path = os.path.join(output_folder, f'{file_id}.xml')
    with open(outfile_path, 'wb') as outfile:
        tree.write(outfile, encoding='utf-8', xml_declaration=True, pretty_print=True)
    return outfile_path


def clean_score_header_and_title(root, metadata):
    """Clean up the score's metadata and title.

    "credit" elements in the original are removed; we use the work and identification metadata instead.
    MuseScore uses the metadata when the credit entires are not there.

    The existing "identification" element is retained but the creator is replaced by the composer from the metadata.

    Desired XML is something like the below. N.B. MusicXML is unnecessairly picky about the order of elements,
    so we have to do a lot of work to reorder elements correctly. For example, work-number can't be after
    work-title or the schema is violated.

      <work>
        <work-number>BWV 869</work-number>
        <work-title>Well-Tempered Clavier Book 1</work-title>
      </work>
      <movement-number>2</movement-number>
      <movement-title>Fugue XII</movement-title>
      <identification>
        <creator type="composer">J.S. Bash</creator>
        ...

    MusicXML docs:
        https://www.musicxml.com/tutorial/file-structure/score-header-entity/
    """
    work_number = metadata['Catalog Number']
    work_title = metadata['Collection']
    movement_num = metadata['Movement Number']

    if isinstance(movement_num, str):
        movement = movement_num
    elif math.isnan(movement_num):
        movement = ''
    else:
        movement = str(movement_num)

    movement_title = metadata['Title']
    composer = metadata['First Name'] + ' ' + metadata['Last Name']

    work = get_child(root, 'work', create_if_missing=True)
    work_number_node = clean_child_text(work, 'work-number', work_number)
    clean_child_text(work, 'work-title', work_title)
    work.insert(0, work_number_node)

    movement_number_node = clean_child_text(root, 'movement-number', movement)
    movement_title_node = clean_child_text(root, 'movement-title', movement_title)

    identification = get_child(root, 'identification', create_if_missing=True)
    creator = clean_child_text(identification, 'creator', composer)
    creator.set('type', 'composer')
    identification.insert(0, creator)
    
    # Now reorder the elements we created.
    root.insert(0, work)
    root.insert(1, movement_number_node)
    root.insert(2, movement_title_node)
    root.insert(3, identification)

    # Delete any "credit" nodes.
    credits = root.findall('credit')
    for credit in credits:
        root.remove(credit)

def clean_part_name(part, metadata_instrument):
    """Clean the part name and remove the part-name-display element.

    MusicXML docs:
        https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-part-name.htm
        https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-part-name-display_1.htm
    """
    part_name = clean_child_text(part, 'part-name', metadata_instrument.name)
    remove_child(part, 'part-name-display')
    return part_name


def clean_score_instrument(part, part_number, metadata_instrument):
    """Returns the new instrument-name node and its id.

    Sets the score-insturment "id" attribute to the given part_number.

    MusicXML docs:
        https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-score-instrument.htm
    """
    score_instrument = get_child(part, 'score-instrument', create_if_missing=True)
    instrument_name = clean_child_text(score_instrument, 'instrument-name', metadata_instrument.name)
    instrument_id = f'P{str(part_number + 1)}-I1'
    score_instrument.set('id', instrument_id)
    return instrument_name, instrument_id


def compute_midi_pan(part_number, num_instruments, border_distance=DEFAULT_PAN_BORDER_DISTANCE, start_from_right=True):
    """Return an appropriate MusicXML pan setting in the range -180 to 180.

    Spreads out instruments based on part_number (0-based)

    Equal space is left between all instruments involved. The distance from the extreme left or right (-90 or 90)
    to the nearest instrument is set by border_distance. Set to 0 to pan hard left/right for 2 instruments,
    or e.g., 30 to make the outer instruments show up at pan -60 and 60.
    """
    if num_instruments == 1:
        return 0
    total_range = 180 - 2 * border_distance
    sep = total_range / (num_instruments - 1)
    if start_from_right:
        return 90 - border_distance - sep * part_number
    else:
        return -90 + border_distance + sep * part_number


def clean_midi_instrument(part, part_number, instrument_id, num_instruments, midi_channel_num, metadata_instrument):
    """Removes virtual instruments, sets midi channels and programs, and sets up volume and stereo panning.

    Uses the id returned by clean_score_instrument to link to a previously-defined score-instrument.

    Assumes that the midi-instrument is only found inside a Part element. Doesn't look for
    sound-element entries within parts.

    Output XML looks something like this:
        <midi-instrument id="P3-I1">
            <midi-channel>3</midi-channel>
            <midi-program>7</midi-program>
            <volume>80</volume>
            <pan>32</pan>
        </midi-instrument>

    MusixXML docs:
        https://usermanuals.musicxml.com/MusicXML/Content/EL-MusicXML-midi-instrument.htm
    """
    midi_instrument = replace_child(part, 'midi-instrument')
    midi_instrument.set('id', instrument_id)

    midi_channel = make_child(midi_instrument, 'midi-channel', midi_channel_num)
    midi_program = make_child(midi_instrument, 'midi-program', metadata_instrument.program)
    volume = make_child(midi_instrument, 'volume', str(DEFAULT_VOLUME + metadata_instrument.volume_boost))
    pan = make_child(midi_instrument, 'pan', str(compute_midi_pan(part_number, num_instruments)))

    return midi_channel, midi_program, pan, volume


def clean_part(part, part_number, num_instruments, midi_channel_num, metadata_instrument):
    """Replaces instrument elements in the part with the desired metadata_instrument.

    See MuxicXML docs:
        https://usermanuals.musicxml.com/MusicXML/Content/CT-MusicXML-score-part.htm
    """
    part_name = clean_part_name(part, metadata_instrument)
    instrument_name, instrument_id = clean_score_instrument(part, part_number, metadata_instrument)
    midi_channel, midi_program, pan, volume = clean_midi_instrument(
        part, part_number, instrument_id, num_instruments, midi_channel_num, metadata_instrument)

    try:
        print(
            f'part: {part_name.text}, instrument: {instrument_name.text}, instrument_id={instrument_id}, '
            f'pan: {pan.text}, volume: {volume.text}, '
            f'midi_channel={midi_channel.text}, midi_program={midi_program.text}')
    except Exception as e:
        print("WARNING: Exception processing part:")
        xpp(part)
        raise e


if __name__ == '__main__':
    main()
