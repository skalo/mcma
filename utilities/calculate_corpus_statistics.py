"""
Calculate basic statistics on the corpus of MusicXML (and possibly MIDI) files in the MCMA

Give a path to the folder with MCMA and launch

"""

import os, glob
from music21 import converter
import pandas as pd
import numpy as np
from collections import Counter


def parse_piece(path_to_piece):
    try:
        score = converter.parse(path_to_piece)
    except converter.ConverterFileException:
        print(f"Skipping file {path_to_piece}: music21 couldn't parse it")
        return None
    characteristics_piece = {}
    characteristics_piece["duration"] = score.duration.quarterLength

    return characteristics_piece


def get_pieces_characteristics(base_folder, composers_collections):
    characteristics = {}
    characteristics["num_pieces"] = 0 # Parse the corpus and find how many pieces there are in general
    characteristics["years"] = set() # How many years apart the pieces are
    characteristics["number_of_pieces_by_parts"] = Counter() # division of the corpus by number of parts
    characteristics["durations"] = [] # all durations of the pieces in music21 sense (quarter note durations)
    characteristics["instruments"] = Counter()

    for composer, collections in composers_collections.items():
        for collection in collections:

            path_to_pieces = os.path.join(base_folder, composer, collection)
            list_of_extensions = ['.mxl', '.xml', '.mid', '.midi']
            pieces = []
            for ext in list_of_extensions:
                pieces +=glob.glob(f'{path_to_pieces}/*{ext}')
            metadata = pd.read_csv(os.path.join(path_to_pieces, "metadata.csv"))
            #some column names have inconsistent case

            metadata.columns = map(str.lower, metadata.columns)
            years = metadata["year"]
            characteristics["years"] |= set(years)

            for instrument_line in metadata["instruments"]:
                instruments = instrument_line.split(';')
                for instrument in instruments:
                    characteristics["instruments"][instrument.strip().lower()]+=1
            for parts in metadata["number of parts"]:
                characteristics["number_of_pieces_by_parts"][parts]+=1
            for path_to_piece in pieces:

                characteristics["num_pieces"] +=1
                characteristics_piece = parse_piece(path_to_piece)
                characteristics["number_of_pieces_by_parts"]["num_parts"] +=1
                characteristics["durations"].append(characteristics_piece["duration"])

    return characteristics

def get_composers_and_collections(base_folder):
    composers = os.listdir(base_folder)
    composers_collections = {}
    for composer in composers:
        collections = os.listdir(os.path.join(base_folder, composer))
        composers_collections[composer] =collections
    return composers_collections


if __name__ == '__main__':
    #give the location of mcma corpus
    base_folder = './mcma'

    composers_collections = get_composers_and_collections(base_folder)
    characteristics = get_pieces_characteristics(base_folder, composers_collections)
    print("##################################")
    print("    MCMA corpus at a glance   ")
    print(f" Distinct composers: {len(composers_collections)}")
    print(f" Number of pieces: {characteristics['num_pieces']} pieces between years {min(characteristics['years'])} and {max(characteristics['years'])}")
    print(f"            Two part: {characteristics['number_of_pieces_by_parts'][2]}")
    print(f"            Three part: {characteristics['number_of_pieces_by_parts'][3]}")
    print(f"            Four part: {characteristics['number_of_pieces_by_parts'][4]}")
    fiveplus = 0
    for i in range(5, 40):
        if(i in characteristics['number_of_pieces_by_parts']):
            fiveplus += characteristics['number_of_pieces_by_parts'][i]
    print(f"            Five and more parts: {fiveplus}\n")

    avg_duration = np.mean(characteristics['durations'])
    std_duration = np.std(characteristics['durations'])
    print(f" Average duration of a piece: {round(avg_duration,2)}+-{round(std_duration,2)} quarter notes, or approximately {round(avg_duration/2, 2)} seconds") #calculate average duration assuming quarter=120 tempo
    print(f" Total duration: {np.sum(characteristics['durations'])} quarter notes")
    print(f" The scores in the corpus contain parts for {len(characteristics['instruments'].items())} distinct instruments:")
    for instr, num in characteristics["instruments"].items():
        print(f"            {instr}: {num} part(s)")

    print("##################################")

