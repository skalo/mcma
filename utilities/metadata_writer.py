"""
Write the metadata for all files in a single folder and save it inside that folder.
To use it, one needs to do four steps:
 - Choose one folder of mcma containing some music (for example, PATH/TO/CLEAN/DATASET/bach_js/goldberg_variations)
 - Fill in the basic_info regarding that music
 - CHANGE the get_info_from_filename function (our dataset is not consistent enough)
 - Run the file
It will write two files in the folder you chose in the first step:
1. The first one is info.json and contains the basic_info dictionary. It is useful to keep as a backup.
2. The second one is metadata.csv and contains the complete metadata with an automatically assigned ID
In case two IDs clash, ONLY A WARNING is issued. The script will produce the files nevertheless.
It will be necessary to manually correct the conflicting IDs.
The script will also try to rename all files according to their IDs. If that's not possible, a warning will be issued.

Once the metadata file is produced, one can open it and manually add information to the Edits and Notes columns.
"""
import csv
import json
import logging
import os
import music21

from collections import defaultdict


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class MetadataWriter:
    def __init__(self, folder, info=None):
        self.folder = folder
        self.columns = [
            'ID',
            'Last Name',
            'First Name',
            'Title',
            'Collection',
            'Catalog Number',
            'Movement Number',
            'Number of Tracks',
            'Instruments',
            'Year',
            'Provenance',
            'Reference Edition',
            'Link to Reference Edition',
            'Comments'
        ]
        self.metadata_path = os.path.join(self.folder, 'metadata.csv')
        self.info_path = os.path.join(self.folder, 'info.json')
        self.info = self._create_info_file(info)

    def _create_info_file(self, info):
        if info is None and not os.path.exists(self.info_path):
            raise RuntimeError("No info file found. Please specify a dictionary with the desired fields.")
        elif info is not None and os.path.exists(self.info_path):
            raise FileExistsError(
                f"{os.path.basename(self.info_path)} already exists. Canceling operation to avoid data loss. "
                f"Please do not pass the info parameter when creating the class to use the existing file "
                f"or remove it manually to force the creation of a new one before trying again."
            )
        elif info is None:
            with open(self.info_path, 'r') as f:
                info = json.load(f)
        else:
            with open(self.info_path, 'w') as f:
                json.dump(info, f, indent=2)
        return defaultdict(lambda: "", info)

    def save_metadata_to_file(self, metadata):
        metadata = sorted(metadata)
        with open(self.metadata_path, 'w') as f:
            writer = csv.writer(f)
            for row in metadata:
                writer.writerow(row)
        logger.info(f"Metadata file successfully saved to {self.metadata_path}")
        return

    def _get_ID(self, row):
        res = []
        last_name = row["Last Name"].replace(' ', '')
        first_name = row["First Name"]
        first_name = ''.join([c for c in first_name if c.isupper()])
        name = last_name + first_name
        res.append(name)
        work = ''.join(filter(lambda ch: ch.isupper() or ch.isnumeric(), row["Catalog Number"]))
        res.append(work)
        if "Movement Number" in row:
            mov = ''.join(filter(lambda ch: ch.isupper() or ch.isnumeric(), row["Movement Number"].title()))
            res.append(mov)
        return '-'.join(res)

    def get_metadata_row(self, f):
        logger.info(f"Getting metadata for file {f}")
        try:
            score = music21.converter.parse(os.path.join(self.folder, f))
        except music21.converter.ConverterFileException:
            logger.info(f"Skipping file {f}: music21 couldn't parse it")
            return None
        row = self.info.copy()
        row["Number of Parts"] = len(score.parts)
        get_info_from_filename(f, row)
        # row["ID"] = self._get_ID(row)
        row["ID"] = f.replace('.mxl', "")
        row["Instruments"] = ';'.join([p.getInstrument().instrumentName for p in score.parts])
        return row

    def rename_file(self, fn_in, fn_out, test):
        src = os.path.join(self.folder, fn_in)
        dst = os.path.join(self.folder, fn_out)
        if os.path.exists(dst):
            logger.warning(f"ID {fn_out} already existing, file {fn_in} has not been renamed")
        else:
            if not test:
                os.rename(src, dst)
            logger.info(f"File moved: {fn_in} -> {fn_out}")
        return

    def run(self, test=False):
        if test:
            logger.warning("Test mode activated. No file will be changed.")
        elif not test and os.path.exists(self.metadata_path):
            raise FileExistsError(
                f"{os.path.basename(self.metadata_path)} already exists. Canceling operation to avoid data loss. "
                f"Please manually modify the file or remove it before trying again."
            )

        files = sorted(os.listdir(self.folder))
        metadata = [self.columns]
        for f in files:
            row = self.get_metadata_row(f)
            if row is None:  # something went wrong when getting the metadata
                continue
            metadata.append([row[c] for c in self.columns])
            self.rename_file(f, row['ID'] + '.mxl', test)

        IDs = [m[self.columns.index("ID")] for m in metadata[1:]]  # remove the header
        if len(IDs) != len(set(IDs)):
            logger.warning("There are conflicting IDs. Please verify manually and change them!")
        if not test:
            self.save_metadata_to_file(metadata)


def get_info_from_filename(f, row):
    """
    This function must change according to the details of the folder we are using.
    It typically modifies the Movement and / or the Work ID fields directly inplace
    :param f: the filename of the piece under study
    :param row: it is a dictionary which keeps the metadata information for f
    :return: nothing, but modifies row in place
    """
    row['Title'] = "Persée LWV 60, Act "
    # logger.info(f"Title: {row['Title']}")
    return


if __name__ == '__main__':
    # Please read instructions at the top of the page
    base_folder = os.path.join('..', 'mcma')
    music_folder = os.path.join(base_folder, 'lully_jb', 'persee')

    # a reminder of the available columns
    # 'ID', 'Last Name', 'First Name', 'Title', 'Collection', 'Catalog Number', 'Movement Number', 'Number of Parts',
    # 'Instruments', 'Year', 'Provenance', 'Reference Edition', 'Link to Reference Edition', 'Notes'
    basic_info = {
        "Last Name": "Lully",
        "First Name": "Jean-Baptiste",
        "Collection": "Persée",
        "Catalog Number": "LWV 60",
        "Year": "1682",
        "Provenance": "Werner Icking Music Collection",
        "Reference Edition": "Damian H. Zanette",
        "Link to Reference Edition": "https://imslp.org/wiki/Special:ReverseLookup/115960",
        "Comments": "Movement separation follows this JC Ballard edition: https://imslp.org/wiki/Special:ReverseLookup/1978"
    }
    basic_info = None
    mw = MetadataWriter(music_folder, basic_info)
    mw.run(test=False)
