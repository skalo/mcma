import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

mpl.rcParams['figure.dpi'] = 250
mpl.rcParams['figure.figsize'] = [3, 4]






folder = '/home/gianlu/PycharmProjects/mcma/mcma/'
data = []
w = os.walk(folder)
for fld, _, file_names in w:
    for f in file_names:
        if f != 'metadata.csv':
            continue
        fp = os.path.join(fld, f)
        data.append(pd.read_csv(fp))
df = pd.concat(data, ignore_index=True)
df = df.sort_values("Last Name")
N = len(df)


# Pie chart
df_nop = df.groupby('Number of Parts').sum()

fig1, ax1 = plt.subplots()
ax1.pie(df_nop, labels=df_nop.index.values, autopct='%1.1f%%', startangle=0)
plt.pie()
centre_circle = plt.Circle((0, 0), 0.90, fc='white')
fig = plt.gcf()
fig.gca().add_artist(centre_circle)
# Equal aspect ratio ensures that pie is drawn as a circle
ax1.axis('equal')
plt.tight_layout()
plt.show()


# # Bar chart
# sns.countplot(x='Last Name', data=df, hue='Number of Parts')
# plt.xlabel("")
# # plt.legend(ncol=3)
# plt.tight_layout()
# plt.show()