from unittest import TestCase

from change_instruments import compute_midi_pan


class TestChangeInstrumentsComputeMidiPan(TestCase):
    def test_compute_midi_pan_1(self):
        pan = compute_midi_pan(0, 1)
        self.assertEqual(0, pan)

    def test_compute_midi_pan_1_border(self):
        pan = compute_midi_pan(1, 1, 30)
        self.assertEqual(0, pan)

    def test_compute_midi_pan_2(self):
        pan = compute_midi_pan(0, 2, 0)
        self.assertEqual(90, pan)

        pan = compute_midi_pan(1, 2, 0)
        self.assertEqual(-90, pan)

    def test_compute_midi_pan_2_border(self):
        pan = compute_midi_pan(0, 2, 30)
        self.assertEqual(60, pan)

        pan = compute_midi_pan(1, 2, 30)
        self.assertEqual(-60, pan)

    def test_compute_midi_pan_3(self):
        pan = compute_midi_pan(0, 3, 0)
        self.assertEqual(90, pan)

        pan = compute_midi_pan(1, 3, 0)
        self.assertEqual(0, pan)

        pan = compute_midi_pan(2, 3, 0)
        self.assertEqual(-90, pan)

    def test_compute_midi_pan_3_border(self):
        pan = compute_midi_pan(0, 3, 30)
        self.assertEqual(60, pan)

        pan = compute_midi_pan(1, 3, 30)
        self.assertEqual(0, pan)

        pan = compute_midi_pan(2, 3, 30)
        self.assertEqual(-60, pan)