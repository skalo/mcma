import unittest
from midi_instrument import get_instrument


class TestMidiInstrument(unittest.TestCase):
    def test_get_instrument_piano(self):
        i = get_instrument('piano')
        self.assertEqual('Piano', i.name)
        self.assertEqual(1, i.program)

    def test_get_instrument_pianoforte(self):
        i = get_instrument('pianoforte')
        self.assertEqual('Piano', i.name, )
        self.assertEqual(1, i.program)

    def test_get_instrument_harpsichord(self):
        i = get_instrument('harpsichord')
        self.assertEqual('Harpsichord', i.name)
        self.assertEqual(7, i.program)

    def test_get_instrument_guitar(self):
        i = get_instrument('guitar')
        self.assertEqual('Guitar', i.name)
        self.assertEqual(25, i.program)

    def test_get_instrument_contrabass(self):
        i = get_instrument('contrabass')
        self.assertEqual('Bass', i.name)
        self.assertEqual(44, i.program)

if __name__ == '__main__':
    unittest.main()
