# MCMA: A Symbolic Multitrack Contrapuntal Music Archive 

The _Multitrack Contrapuntal Music Archive_ (**MCMA**) is a symbolic dataset of pieces specifically collated and edited to comprise, for any given polyphonic work, independent parts.

We would like to open **MCMA** for contributions, in the hope that it can continue to grow beyond our efforts. 

Detailed instructions regarding **MCMA**, the metadata protocol, how to contribute, and how to use the utilities can be found in the 
[Documentation](https://mcma.readthedocs.io/), where it is also possible to download the current dataset as .zip file.


## Contents
- **docs**<br>Docs directory for the documentation website.
- **mcma**<br>The music archive itself. Use this for your projects (generative music, musicological analysis, etc.).
- **utilities**<br>Goodies that might come in handy.


## How to Cite
Aljanaki A., Kalonaris S., Micchi G. & Nichols E. (2021) MCMA: A Symbolic Multitrack Contrapuntal Music Archive. _Empirical Musicology Review_. Forthcoming.