# Building the docs

## Install sphinx and the ReadTheDocs theme:
pip install sphinx
pip install sphinx_rtd_theme
pip install recommonmark

## Build the doc HTML files
make html
