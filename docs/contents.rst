.. Multitrack Contrapuntal Music Archive documentation master file, created by
   sphinx-quickstart on Fri Apr 10 21:48:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Multitrack Contrapuntal Music Archive (MCMA)
=================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   docs/about
   docs/download
   docs/collections
   docs/guidelines


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
