# Download MCMA

There are two options to download the dataset.

## Zip File

Download the latest zip file here: [mcma-master.zip](https://gitlab.com/skalo/mcma/-/archive/master/mcma-master.zip?path=mcma)

## Clone Gitlab Repo

Clone the [MCMA gitlab repo](https://gitlab.com/skalo/mcma). This is recommended for those who want to contribute to the project.

```git clone https://gitlab.com/skalo/mcma.git```
