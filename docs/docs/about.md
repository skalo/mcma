# About

We present _Multitrack Contrapuntal Music Archive_ (**MCMA**), a symbolic dataset of pieces specifically collated and edited to comprise, for any given polyphonic work, independent parts.

**MCMA** is geared towards musicological tasks such as (computational) analysis or education, as it brings to the fore contrapuntal interactions by explicit and independent representation. Empirically analyzing counterpoint music is nearly impossible without voice separation (e.g., by means of “exploding” a part into its constituent voices). Using **MCMA**, each part can be analyzed separately, which offers a unique perspective for investigating voice leading in counterpoint. A musicologist would be able to quickly find examples of retrograde inversions or augmentations in counterpoint melodic lines, or find statistically valid answers for their questions: for example, what is the most common interval to use for parallel contrapuntal motion? Is contrary motion preferred to other types of motion?

Below, an example of multitrack expansion: the first three bars of J.S. Bach's Fugue No.3 in C# Major BWV 872, from the Well-Tempered Clavier, Book II, (top) against the same extract in MCMA’s corpus (bottom).

![Original](imgs/BachJS-BWV872-F_bars_1-3-original.png)

![Multitrack](imgs/BachJS-BWV872-F_bars_1-3-multitrack.png)


Moreover, **MCMA** can be useful in the context of generative systems and machine learning. For example, the relation between two parts in a contrapuntal piece becomes paramount if one wants to employ natural language processing models, such as attention networks, and treats one part as the musical response to a “query”.
Below, an example of this approach: the first two bars of J.S.Bach’s first two-part inventions.

![Query-Answer](imgs/bwv_772_First_2_bars-QA-cropped.jpg)

## FAIR
 MCMA is [FAIR](https://www.go-fair.org/fair-principles/)-compliant. It has an extensive metadata protocol (F2), which is described in detail in the **Metadata** section of the [Guidelines for Contributors](guidelines.md). The metadata is registered in a comma separated values file (F4, A1, A1.1): each music work is assigned a globally unique and persistent identifier (F1) and is richly described with a plurality of accurate and relevant attributes (R1).

## Contribution Call
We open **MCMA** to contributions, in the hope that **MCMA** can continue to grow beyond our efforts. Please see the [Guidelines for Contributors](guidelines.md) section.


If you wish to simply use **MCMA** without contributing, we provide [instructions](download.md) on how to download the current dataset as .zip file.

## How to Cite
Aljanaki A., Kalonaris S., Micchi G. & Nichols E. (2021) MCMA: A Symbolic Multitrack Contrapuntal Music Archive. _Empirical Musicology Review_. Forthcoming.
