# Collections in MCMA

The following table gives the number of pieces from each collection that is currently included in MCMA.

| Composer    | Collection                         | Number of Pieces |
|-------------|------------------------------------|------------------|
| Albinoni    |                                    |                  |
|             | Sonate da chiesa Op. 4             | 24               |
|             | Trattenimenti armonici Op. 6       | 48               |
|             | Subtotal                           | 72               |
| Bach        |                                    |                  |
|             | Goldberg Variations                | 31               |
|             | Inventions                         | 15               |
|             | Kunst der Fuge                     | 21               |
|             | Sinfonias                          | 15               |
|             | The Well-Tempered Clavier, Book I  | 48               |
|             | The Well-Tempered Clavier, Book II | 48               |
|             | Subtotal                           | 178              |
| Becker      |                                    |                  |
|             | Sonatas and Suites                 | 45               |
|             | Subtotal                           | 45               |
| Buxtehude   |                                    |                  |
|             | Trio Sonatas Op.1                  | 45               |
|             | Trio Sonatas Op.2                  | 43               |
|             | Subtotal                           | 88               |
| Lully       |                                    |                  |
|             | Persée                             | 92               |
|             | Subtotal                           | 92               |
| Grand Total |                                    | 475              |